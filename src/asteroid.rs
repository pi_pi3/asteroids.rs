use rand::prelude::*;

use amethyst::prelude::*;
use amethyst::assets::{AssetStorage, Loader};
use amethyst::core::{
    transform::Transform,
    specs::{Read, SystemData, Resources},
    shrev::{ReaderId, EventChannel},
    EventReader
};
use amethyst::ecs::prelude::{Component, VecStorage, DenseVecStorage};
use amethyst::renderer::{
    Camera, PngFormat, Projection, SpriteRender, SpriteSheet,
    SpriteSheetFormat, SpriteSheetHandle, Texture, TextureMetadata,
    Event, VirtualKeyCode
};
use amethyst::ui::UiEvent;
use amethyst::input::{is_close_requested, is_key_down};
use amethyst::derive::EventReader;

pub const WIDTH: f32 = 360.0;
pub const HEIGHT: f32 = 240.0;
pub const ASTEROIDS: usize = 16;
pub const SHIP_RADIUS: f32 = 12.0;
pub const ASTEROID_RADIUS: f32 = 4.0;
pub const BULLET_RADIUS: f32 = 5.0;
pub const SCALE: f32 = 0.25;

#[derive(Clone, Copy, PartialEq, Component)]
#[storage(DenseVecStorage)]
pub struct Physics {
    pub x: f32,
    pub y: f32,
    pub rot: f32,
    pub vx: f32,
    pub vy: f32,
    pub vrot: f32,
    pub ax: f32,
    pub ay: f32,
    pub arot: f32,
    pub friction: f32,
}

impl Default for Physics {
    fn default() -> Self {
        Physics {
            x: 0.0,
            y: 0.0,
            rot: 0.0,
            vx: 0.0,
            vy: 0.0,
            vrot: 0.0,
            ax: 0.0,
            ay: 0.0,
            arot: 0.0,
            friction: 1.0,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Default, Component)]
#[storage(VecStorage)]
pub struct Ship {
    pub shoot_timer: f32,
}

#[derive(Clone, Copy, PartialEq, Eq, Component)]
#[storage(VecStorage)]
pub struct Asteroid {
    pub size: usize,
}

impl Default for Asteroid {
    fn default() -> Self {
        Asteroid { size: 3 }
    }
}

#[derive(Clone, Copy, PartialEq, Default, Component)]
#[storage(VecStorage)]
pub struct Bullet;

#[derive(Clone, Copy, PartialEq, Default, Component)]
#[storage(VecStorage)]
pub struct Collider {
    pub radius: f32,
}

fn initialise_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_z(1.0);
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(0.0, WIDTH, 0.0, HEIGHT)))
        .with(transform)
        .build();
}

fn initialise_ship(world: &mut World, sprite_sheet: &SpriteSheetHandle) {
    let mut pos_scale = Transform::default();
    let x = WIDTH * 0.5;
    let y = HEIGHT * 0.5;
    pos_scale.set_xyz(x, y, 0.0);
    pos_scale.set_scale(SCALE, SCALE, 1.0);

    let sprite = SpriteRender {
        sprite_sheet: sprite_sheet.clone(),
        sprite_number: 0, // ship is the first sprite in the sprite_sheet
    };

    // ship
    world
        .create_entity()
        .with(Ship::default())
        .with(pos_scale)
        .with(Physics {
            x,
            y,
            friction: 1.01,
            ..Default::default()
        })
        .with(sprite.clone())
        .with(Collider {
            radius: SCALE * SHIP_RADIUS,
        })
        .with(sprite_sheet.clone())
        .build();
}

fn initialise_asteroid(world: &mut World, sprite_sheet: &SpriteSheetHandle) {
    let mut pos_scale = Transform::default();
    let x = random::<f32>() * WIDTH;
    let y = random::<f32>() * HEIGHT;
    pos_scale.set_xyz(x, y, 0.0);
    pos_scale.set_scale(SCALE, SCALE, 1.0);

    let physics = Physics {
        x,
        y,
        rot: 0.0,
        vx: random::<f32>() * 200.0 - 100.0,
        vy: random::<f32>() * 200.0 - 100.0,
        vrot: random::<f32>() * std::f32::consts::PI * 0.01 * 100.0,
        ..Default::default()
    };

    let size = random::<usize>() % 3 + 1;

    let sprite = SpriteRender {
        sprite_sheet: sprite_sheet.clone(),
        sprite_number: size,
    };

    // random asteroid
    world
        .create_entity()
        .with(Asteroid { size })
        .with(pos_scale)
        .with(physics)
        .with(sprite.clone())
        .with(Collider {
            radius: SCALE * ASTEROID_RADIUS * size as f32,
        })
        .with(sprite_sheet.clone())
        .build();
}

fn load_sprite_sheet(world: &mut World) -> SpriteSheetHandle {
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/spritesheet.png",
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "texture/spritesheet.ron", // Here we load the associated ron file
        SpriteSheetFormat,
        texture_handle,
        (),
        &sprite_sheet_store,
    )
}

#[derive(Clone, Copy, Debug)]
pub enum AppEvent {
    GameOver,
    Restart,
}

#[derive(Clone, Debug, EventReader)]
#[reader(PlayReader)]
pub enum PlayEvent {
    Window(Event),
    Ui(UiEvent),
    AppEvent(AppEvent),
}

pub struct PlayState {
    reader: Option<ReaderId<AppEvent>>,
}

impl Default for PlayState {
    fn default() -> PlayState {
        PlayState { reader: None }
    }
}

impl<'a, 'b> State<GameData<'a, 'b>, PlayEvent> for PlayState {
    fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;
        world.delete_all();
        world.add_resource(EventChannel::<AppEvent>::new());
        self.reader = Some(world.res.fetch_mut::<EventChannel<AppEvent>>().register_reader());

        let sprite_sheet_handle = load_sprite_sheet(world);

        initialise_camera(world);
        initialise_ship(world, &sprite_sheet_handle);

        for _ in 0..ASTEROIDS {
            initialise_asteroid(world, &sprite_sheet_handle);
        }
    }

    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, PlayEvent> {
        data.world.maintain();
        data.data.update(&data.world);

        let channel = data.world.res.fetch::<EventChannel<AppEvent>>();
        #[allow(clippy::never_loop)] // clippy falsely identifies this
        for ev in channel.read(self.reader.as_mut().unwrap()) {
            match *ev {
                AppEvent::GameOver => return Trans::Switch(Box::new(GameOverState)),
                AppEvent::Restart => return Trans::Switch(Box::new(PlayState::default())),
            }
        }

        Trans::None
    }

    fn handle_event(&mut self, _data: StateData<GameData>, event: PlayEvent) -> Trans<GameData<'a, 'b>, PlayEvent> {
        match event {
            PlayEvent::AppEvent(AppEvent::GameOver) => Trans::Switch(Box::new(GameOverState)),
            PlayEvent::AppEvent(AppEvent::Restart) => Trans::Switch(Box::new(PlayState::default())),
            PlayEvent::Window(ref event) if is_close_requested(event) => Trans::Quit,
            PlayEvent::Window(ref event) if is_key_down(event, VirtualKeyCode::R) => {
                Trans::Switch(Box::new(PlayState::default()))
            }
            _ => Trans::None,
        }
    }
}

pub struct GameOverState;

impl<'a, 'b> State<GameData<'a, 'b>, PlayEvent> for GameOverState {
    fn on_start(&mut self, _data: StateData<GameData>) {
        println!("Game over!");
    }

    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, PlayEvent> {
        data.data.update(&data.world);
        Trans::None
    }

    fn handle_event(&mut self, _data: StateData<GameData>, event: PlayEvent) -> Trans<GameData<'a, 'b>, PlayEvent> {
        match event {
            PlayEvent::Window(ref event) if is_close_requested(event) => Trans::Quit,
            PlayEvent::Window(ref event) if is_key_down(event, VirtualKeyCode::R) => {
                Trans::Switch(Box::new(PlayState::default()))
            }
            PlayEvent::AppEvent(AppEvent::Restart) => Trans::Switch(Box::new(PlayState::default())),
            _ => Trans::None,
        }
    }
}
