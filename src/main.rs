extern crate rand;
extern crate amethyst;
#[macro_use]
extern crate specs_derive;

use amethyst::{
    prelude::*,
    renderer::{DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage,
                ColorMask, ALPHA},
    utils::application_root_dir,
    core::transform::TransformBundle,
    input::InputBundle,
};

use self::asteroid::{PlayEvent, PlayReader, PlayState};

pub mod systems;
pub mod asteroid;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir();

    let display_config = format!("{}/resources/display_config.ron", app_root);
    let config = DisplayConfig::load(&display_config);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawFlat2D::new().with_transparency(ColorMask::all(), ALPHA, None)),
    );

    let binding_path = if cfg!(feature = "sdl_controller") {
        format!("{}/resources/controller_config.ron", application_root_dir())
    } else {
        format!("{}/resources/keyboard_config.ron", application_root_dir())
    };

    let input_bundle = InputBundle::<String, String>::new().with_bindings_from_file(binding_path)?;

    let game_data = GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
        .with_bundle(TransformBundle::new())?
        .with_bundle(input_bundle)?
        .with(systems::ShipSystem, "ship_system", &["input_system"])
        .with(systems::PhysicsSystem, "physics_system", &[])
        .with(systems::CollisionSystem, "collision_system", &["physics_system"]);

    let assets_path = format!("{}/assets/", app_root);
    let mut game =
        CoreApplication::<GameData, PlayEvent, PlayReader>::new(&assets_path, PlayState::default(), game_data)?;

    game.run();

    Ok(())
}
