use rand::prelude::*;

use amethyst::core::Transform;
use amethyst::ecs::{Join, Read, ReadStorage, System, Write, WriteStorage, Entities, LazyUpdate};
use amethyst::renderer::{SpriteRender, SpriteSheetHandle};
use amethyst::shrev::EventChannel;

use asteroid::{
    Physics, Ship, Collider, Bullet, Asteroid, SCALE, ASTEROID_RADIUS,
    AppEvent,
};

pub struct CollisionSystem;

type CollisionSystemData<'s> = (
    ReadStorage<'s, Collider>,
    ReadStorage<'s, Ship>,
    WriteStorage<'s, Asteroid>,
    ReadStorage<'s, Bullet>,
    ReadStorage<'s, SpriteSheetHandle>,
    ReadStorage<'s, Physics>,
    Entities<'s>,
    Read<'s, LazyUpdate>,
    Write<'s, EventChannel<AppEvent>>,
);

impl<'s> System<'s> for CollisionSystem {
    // no system should have this much access
    type SystemData = CollisionSystemData<'s>;

    fn run(
        &mut self,
        (
            colliders,
            ships,
            mut asteroids,
            bullets,
            sprite_sheets,
            physics,
            entities,
            updater,
            mut events,
        ): Self::SystemData,
    ) {
        {
            let ship = (&entities, &colliders, &physics, &ships).join().collect::<Vec<_>>();
            let asteroid = (&colliders, &physics, &asteroids).join();

            for (asteroid, a, _) in asteroid {
                for (entity_ship, ship, b, _) in &ship {
                    let d = (b.x - a.x).hypot(b.y - a.y);
                    let collision = d <= asteroid.radius + ship.radius;
                    if collision {
                        entities.delete(*entity_ship).unwrap();
                        events.single_write(AppEvent::GameOver);
                    }
                }
            }
        }

        {
            let bullet = (&entities, &colliders, &physics, &bullets).join().collect::<Vec<_>>();
            let asteroid = (&entities, &colliders, &physics, &mut asteroids, &sprite_sheets).join();

            for (entity_a, asteroid, a, asteroid_tag, sprite_sheet) in asteroid {
                for (entity_b, bullet, b, _) in &bullet {
                    let d = (b.x - a.x).hypot(b.y - a.y);
                    let collision = d <= asteroid.radius + bullet.radius;
                    if collision && asteroid_tag.size > 0 {
                        asteroid_tag.size -= 1;
                        if asteroid_tag.size > 0 {
                            for _ in 0..2 {
                                let mut pos_scale = Transform::default();
                                pos_scale.set_xyz(a.x, a.y, 0.0);
                                pos_scale.set_scale(SCALE, SCALE, 1.0);

                                let body = Physics {
                                    x: a.x,
                                    y: a.y,
                                    vx: random::<f32>() * 200.0 - 100.0,
                                    vy: random::<f32>() * 200.0 - 100.0,
                                    vrot: random::<f32>() * std::f32::consts::PI * 0.01 * 100.0,
                                    ..Default::default()
                                };

                                let sprite = SpriteRender {
                                    sprite_sheet: sprite_sheet.clone(),
                                    sprite_number: asteroid_tag.size,
                                };

                                // random asteroid
                                let asteroid = entities.create();
                                updater.insert(
                                    asteroid,
                                    Asteroid {
                                        size: asteroid_tag.size,
                                    },
                                );
                                updater.insert(asteroid, pos_scale);
                                updater.insert(asteroid, body);
                                updater.insert(asteroid, sprite.clone());
                                updater.insert(
                                    asteroid,
                                    Collider {
                                        radius: SCALE * ASTEROID_RADIUS * asteroid_tag.size as f32,
                                    },
                                );
                                updater.insert(asteroid, sprite_sheet.clone());
                            }
                        }
                        entities.delete(entity_a).unwrap();
                        entities.delete(*entity_b).unwrap();
                    }
                }
            }
        }
    }
}
