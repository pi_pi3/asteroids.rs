use amethyst::core::Transform;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, System, Read, WriteStorage};

use asteroid::{Physics, WIDTH, HEIGHT};

pub const MAX_VX: f32 = 500.0;
pub const MAX_VY: f32 = 500.0;
pub const MAX_VROT: f32 = 1500.0 * std::f32::consts::PI;

pub struct PhysicsSystem;

impl<'s> System<'s> for PhysicsSystem {
    type SystemData = (WriteStorage<'s, Transform>, WriteStorage<'s, Physics>, Read<'s, Time>);

    fn run(&mut self, (mut transforms, mut physics, time): Self::SystemData) {
        for (physics, transform) in (&mut physics, &mut transforms).join() {
            physics.rot += physics.vrot * time.delta_seconds();
            physics.x += physics.vx * time.delta_seconds();
            physics.y += physics.vy * time.delta_seconds();

            transform.set_rotation_euler(0.0, physics.rot, 0.0);
            transform.set_x(physics.x);
            transform.set_y(physics.y);

            if physics.x < 0.0 {
                physics.x = WIDTH;
            }
            if physics.y < 0.0 {
                physics.y = HEIGHT;
            }
            if physics.x > WIDTH {
                physics.x = 0.0;
            }
            if physics.y > HEIGHT {
                physics.y = 0.0;
            }

            physics.vrot += physics.arot * time.delta_seconds();
            physics.vx += physics.ax * time.delta_seconds();
            physics.vy += physics.ay * time.delta_seconds();

            if physics.vrot.abs() > MAX_VROT {
                physics.vrot = physics.vrot.signum() * MAX_VROT;
            }

            if physics.vx.abs() > MAX_VX {
                physics.vx = physics.vx.signum() * MAX_VX;
            }

            if physics.vy.abs() > MAX_VY {
                physics.vy = physics.vy.signum() * MAX_VY;
            }

            physics.vrot /= physics.friction;
            physics.vx /= physics.friction;
            physics.vy /= physics.friction;

            physics.arot /= physics.friction;
            physics.ax /= physics.friction;
            physics.ay /= physics.friction;
        }
    }
}
