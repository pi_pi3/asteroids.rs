use amethyst::core::Transform;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, Read, ReadStorage, System, WriteStorage, Entities, LazyUpdate};
use amethyst::input::InputHandler;
use amethyst::renderer::{SpriteRender, SpriteSheetHandle};

use asteroid::{Physics, Ship, Collider, Bullet, SCALE, BULLET_RADIUS};

pub const ACCEL: f32 = 5.0;
pub const SHOOT_DELAY: f32 = 0.2;
pub const BULLET_VEL: f32 = 100.0;

pub struct ShipSystem;

type ShipSystemData<'s> = (
    WriteStorage<'s, Physics>,
    WriteStorage<'s, Ship>,
    ReadStorage<'s, SpriteSheetHandle>,
    Read<'s, InputHandler<String, String>>,
    Read<'s, Time>,
    Entities<'s>,
    Read<'s, LazyUpdate>,
);

impl<'s> System<'s> for ShipSystem {
    type SystemData = ShipSystemData<'s>;

    fn run(&mut self, (mut physics, mut ships, sprite_sheets, input, time, entities, updater): Self::SystemData) {
        for (body, ship, sprite_sheet) in (&mut physics, &mut ships, &sprite_sheets).join() {
            // moving
            let rotate = input.axis_value("rotate");
            let thrust = input.axis_value("thrust");

            if let Some(rotate) = rotate {
                body.arot = rotate.to_radians() as f32 * 750.0;
            }

            if let Some(thrust) = thrust {
                let thrust = thrust.max(0.0) * 100.0;
                body.ax = -thrust as f32 * body.rot.sin() * ACCEL;
                body.ay = thrust as f32 * body.rot.cos() * ACCEL;
            }

            let x = body.x;
            let y = body.y;
            let rot = body.rot;

            // shooting
            if ship.shoot_timer > 0.0 {
                ship.shoot_timer -= time.delta_seconds();
            }

            let shoot = input.action_is_down("shoot");

            if let Some(shoot) = shoot {
                if shoot && ship.shoot_timer <= 0.0 {
                    ship.shoot_timer = SHOOT_DELAY;

                    let mut pos_scale = Transform::default();
                    pos_scale.set_xyz(x, y, 0.0);
                    pos_scale.set_scale(SCALE, SCALE, 1.0);

                    let sprite = SpriteRender {
                        sprite_sheet: sprite_sheet.clone(),
                        sprite_number: 4,
                    };

                    let vx = -rot.sin() * BULLET_VEL;
                    let vy = rot.cos() * BULLET_VEL;

                    let bullet = entities.create();
                    updater.insert(bullet, Bullet);
                    updater.insert(bullet, pos_scale);
                    updater.insert(
                        bullet,
                        Physics {
                            x,
                            y,
                            vx,
                            vy,
                            ..Default::default()
                        },
                    );
                    updater.insert(bullet, sprite.clone());
                    updater.insert(
                        bullet,
                        Collider {
                            radius: SCALE * BULLET_RADIUS,
                        },
                    );
                }
            }
        }
    }
}
